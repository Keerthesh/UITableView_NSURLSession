To complete my Writing Exercise assignment, I have considered to update this file (README.md) itself with the complete project coding and usage details.

/* Background */

All started with the mail from Mr. Venkat informing that my profile has been shortlisted for 'iOS Architect' role and as part of the process I need to 
complete 2 assignment i,e Writing exercise and Coding exercise. I have completed my coding excercise and shared the code link through GitLab repository
with the public access. 

/* Assignment requirement and the platform on which needs to be developed details are already published in ios.md file */

/* Project Implementation */
As part of this project implementation, I have covered "MVC" and "Sigleton" architecture patterns. Project workspace mainly contains modules as mentioned
below 
1) 2 View controllers
2) 1 Main Storyboard and 1 xib for interface
3) 1 Model file to store all the user details.
4) 1 Service module to accomplish transport layer in the application.
5) Core data for data storing on device.

Explanation of each of the above mentioned components is as follows
1) ViewController.swift -> This is the landing page of our application, which is meant for receiving the "mailid" as an input for the user which is
                            mandatory parameter to be sent as part of HTTP POST body to receive the response from the server.

                            This file also includes validation method for the mailid received from the user and to show the 
                            relevant alert messages.
                            
2) UserTableViewController.swift -> As name indicates this is a table view which is responsible for showing the all the user 
                            related data like user photo, name and mail id. Core data fetchedresultscontroller is used as 
                            datasource for this tableview and NSCache for cacheing the images.
                            
3) Storyboard and XIB   -> These are user interface which mainly meant for the views and related visual components. This will 
                            be associated with the controller component to handle all the related functionality. 
                            
4) Model                -> This model is inherited from NSManagedObject model which represents an object in the Users Entity of the 
                            Core data.
                            
5) Service              -> This is the important module of this application, which takes care of fetching all the user details
                            from the server and updating it to the core data. This module includes files like 
                            a) Request.swift  -> This is the generic singleton class which mainly uses NSURLSession for 
                                                network operations. This includes methods like downloadDataFromURL (for fetching
                                                the response ), getJSONFromData (for converting data to JSON format), dataSourceBaseURLString
                                                (for framing the base URL).
                                                This receives the completion handler has an input from the caller class and invokes the same 
                                                once the download task is completed. 
                                                
                            b) UserRequest.swift -> This is specific to the functionality which acts a middle layer between View 
                                                Controller and Request.swift modules. This includes all the necessary information related
                                                to User functionality and sends as an input to Request.swift methods. These kind of files will
                                                be added to achieve different functionality. For example Authentication.swift for handling all the
                                                authentication related functionalities like login, logout and sign up.
                                                
                            c) datasource.plist -> This is the configuration file which stores all the network related configuration. 
                                                In this application, I have store all the base URL related to configuration and framing at 
                                                runtime in Request.swift.

/* Application Usage */
Run the application in simulator, the landing page requesting for valid emailid will be shown. User can input the valid emailid and click on submit. 
Application will navigate to table view controller with all the necessary information of the users.

/* Important features/components covered as part of the Application */
1. Storyboard and XIB -> User interfaces. 
2. Implemented MVC and Sigleton.
3, NSURLSession with completion handlers.
4. NSURLCache -> To store images. Memory will be automatically perged by the OS if there is memory shortage to run the app.

/* Scope for improvements */
There are many based on availablity of time
1. Can be implemented for all the devices with all orientation support - Using autolayout can be helpful.
2. Error handling as part of the completion handler. 
3. Utility function to show alerts in the one place. 
4. Usage of GCD for background tasks.

/* Conclusion */
Overall it was very good experience with both writing and coding exercises. Looking forward for your valuable feedback.
                                                

