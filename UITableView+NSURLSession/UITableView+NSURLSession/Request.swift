//
//  Request.swift
//  UITableView+NSURLSession
//
//  Created by Keerthesh on 29/06/16.
//  Copyright © 2016 Keerthesh. All rights reserved.
//

import Foundation

class Request {
    
    class var sharedInstance: Request {
        
        struct Static {
            static let instance: Request = Request()
        }
        return Static.instance
    }
    
    func downloadDataFromURL(requestURL: NSURL, type: String, httpBody: String, completionHandler: (resultData: NSData, error: NSError?) -> Void) {
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration)
        
        let request = NSMutableURLRequest(URL: requestURL)
        request.HTTPMethod = type
        if request.HTTPMethod == "POST" {
            
            request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
        }
        
        let task = session.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            
            if error != nil {
                print("Error in downloading data \(error?.description)")
                
            } else {
                NSOperationQueue.mainQueue().addOperationWithBlock({ 
                    completionHandler(resultData: data!, error: nil)
                })
            }
        })
        task.resume()
    }

    func getJSONFromData(resultData: NSData) -> Dictionary<String, AnyObject>{
        
        var decodedJson: Dictionary<String, AnyObject> = [:]
        do {
           decodedJson = try NSJSONSerialization.JSONObjectWithData(resultData, options: .AllowFragments) as! Dictionary<String, AnyObject>
            
        } catch let error as NSError {
            print(error)
        }
        return decodedJson
    }
    
    var dataSourceBaseURLString: String {
        
        guard let dataSourcePath = NSBundle.mainBundle().pathForResource("datasource", ofType: "plist"),
            let params = NSDictionary(contentsOfFile: dataSourcePath),
            let proto = params["protocol"],
            let host = params["host"] else {
                fatalError("Cannot find the 'datasource.plist' or required parameters")
        }
        return "\(proto)://\(host)/"
    }
    
}

