//
//  UsersTableViewController.swift
//  UITableView+NSURLSession
//
//  Created by Keerthesh on 30/06/16.
//  Copyright © 2016 Keerthesh. All rights reserved.
//

import UIKit
import CoreData

class UsersTableViewController: UITableViewController {
    
    
    func performFetch() {
        do {
            try self.fetchedResultsController.performFetch()
        } catch  {
            print ("Perform Fetch is getting failed")
        }
        self.tableView.reloadData()
    }
    
    private var fetchedResultsController: NSFetchedResultsController = {
        
        let request = User.fetchRequest()
        var sortAlpha = NSSortDescriptor(key: "firstName", ascending: true)
        request.sortDescriptors = [sortAlpha]
        let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultsController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView?.registerNib(UINib(nibName: "UsersTableViewCell", bundle: nil), forCellReuseIdentifier: "UsersTableViewCellReuseId")
        self.navigationItem.title = "Users"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        guard let sectionCount = self.fetchedResultsController.sections?.count else {
            return 0
        }
        return sectionCount
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let numberOfRows = self.fetchedResultsController.sections![section].numberOfObjects
        return numberOfRows
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80.0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UsersTableViewCellReuseId", forIndexPath: indexPath) as? UsersTableViewCell
        
        let user = fetchedResultsController.objectAtIndexPath(indexPath) as? User
        cell?.firstName?.text = user?.firstName
        cell?.lastName?.text = user?.lastName
        cell?.mailId?.text = user?.emailId
        cell?.userimage?.image = UIImage()
        
        let image = imageCache.objectForKey((user?.imageUrl)!) as? UIImage
        if image != nil {
            cell?.userimage?.image = image
        } else {
        
            print("Firstname is \(cell?.firstName?.text)")
            let url = NSURL(string: (user?.imageUrl!)!)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                guard let data = NSData(contentsOfURL: url!) else {
//                    print("Data is nil for \(cell?.firstName?.text)")
                    cell?.userimage?.image = UIImage()
                    return
                }
                dispatch_async(dispatch_get_main_queue(), {
                    guard let image = UIImage(data: data) else {
                        print("Image is nil for \(cell?.firstName?.text)")
                        return
                    }
                    imageCache.setObject(image, forKey: (user?.imageUrl)!)
                    cell?.userimage?.image = image
                })
            }
        }
        return cell!
        
        //        cell?.userimage?.image = loadImageFromURL((user?.imageUrl)!)
    }
    
}
