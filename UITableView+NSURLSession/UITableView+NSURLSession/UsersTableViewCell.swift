//
//  UsersTableViewCell.swift
//  UITableView+NSURLSession
//
//  Created by Keerthesh on 02/07/16.
//  Copyright © 2016 Keerthesh. All rights reserved.
//

import UIKit

class UsersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var firstName: UILabel?
    @IBOutlet weak var lastName: UILabel?
    @IBOutlet weak var mailId: UILabel?
    @IBOutlet weak var userimage: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
