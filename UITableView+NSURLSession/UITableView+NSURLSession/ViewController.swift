//
//  ViewController.swift
//  UITableView+NSURLSession
//
//  Created by Keerthesh on 28/06/16.
//  Copyright © 2016 Keerthesh. All rights reserved.
//

import UIKit

var imageCache = NSCache()

class ViewController: UIViewController {
    
    @IBOutlet weak var textlbl: UILabel?
    @IBOutlet weak var emailField: UITextField?
    
    var userTableViewController:UsersTableViewController?
    var userRequestObj:UserRequest = UserRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.title = "Project Assignment"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitPressed(sender: AnyObject) {
        
        let mailId = emailField?.text!
        if validateEmail(mailId!) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            userTableViewController = storyboard.instantiateViewControllerWithIdentifier("UsersTableViewControllerReuseId") as? UsersTableViewController
            
            invokeGetUsersList()
            
            self.navigationController?.pushViewController(userTableViewController!, animated: true)
            
        } else {
            
            let alert = UIAlertController(title: "Email Validation", message: "Please enter valid email address", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        emailField?.resignFirstResponder()
        emailField?.text = ""
    }
    
    private func invokeGetUsersList() {
        
        let completionHandler = {(error:NSError?) -> Void in
            self.userTableViewController?.performFetch()
        }
        
        userRequestObj.getUsersList("a@b.com", completionHandler: completionHandler)
    }
    
}

