//
//  Common.swift
//  UITableView+NSURLSession
//
//  Created by Keerthesh on 02/07/16.
//  Copyright © 2016 Keerthesh. All rights reserved.
//

import Foundation
import UIKit

func delay(delay: Double, closure: ()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Double(NSEC_PER_SEC))
        ),
        dispatch_get_main_queue(), closure)
}

func loadImageFromURL(urlString: String) -> UIImage {
    let url = NSURL(string: urlString)
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
        let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
        dispatch_async(dispatch_get_main_queue(), {
            return UIImage(data: data!)
        })
    }
    return UIImage()
}

func validateEmail(mailID: String) -> Bool {
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluateWithObject(mailID)
}