//
//  User+CoreDataProperties.swift
//  UITableView+NSURLSession
//
//  Created by Keerthesh on 29/06/16.
//  Copyright © 2016 Keerthesh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var emailId: String?
    @NSManaged var firstName: String?
    @NSManaged var imageUrl: String?
    @NSManaged var lastName: String?

}
