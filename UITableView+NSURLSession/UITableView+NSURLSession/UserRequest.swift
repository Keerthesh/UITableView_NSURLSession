//
//  UserRequest.swift
//  UITableView+NSURLSession
//
//  Created by Keerthesh on 02/07/16.
//  Copyright © 2016 Keerthesh. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class UserRequest {
    
//    func getUsersList(mailId: String, completionHandler: (result: Array<Dictionary<String, String>>) -> Void) {
//    
//        let params = "{" + "\"emailId\"" + ":" + "\"\(mailId)\"" + "}"
//        let usersURL = NSURL(string:"\(Request.sharedInstance.dataSourceBaseURLString)list")
//        
//        let completionHandler = {(resultData:NSData) in
//            let usersResponse = Request.sharedInstance.getJSONFromData(resultData)
//            guard let usersList = usersResponse["items"] as? Array<Dictionary<String, String>> else {
//                print("No results found")
//                return
//            }
//            delay(0.0, closure: {
//                completionHandler(result: usersList)
//            })
//        }
//        Request.sharedInstance.downloadDataFromURL(usersURL!, type: "POST", httpBody: params, completionHandler:completionHandler)
//    }
    
    func getUsersList(mailId: String, completionHandler: (error:NSError?) -> Void) {
        
        let params = "{" + "\"emailId\"" + ":" + "\"\(mailId)\"" + "}"
        let usersURL = NSURL(string:"\(Request.sharedInstance.dataSourceBaseURLString)list")
        
        let completionHandler = {(resultData:NSData, error:NSError?) in
            let usersResponse = Request.sharedInstance.getJSONFromData(resultData)
            guard let usersList = usersResponse["items"] as? Array<Dictionary<String, String>> else {
                print("No results found")
                return
            }
            delay(0.0, closure: {
                self.updateUserToCoreData(usersList, completionHandler: completionHandler)
            })
        }
        Request.sharedInstance.downloadDataFromURL(usersURL!, type: "POST", httpBody: params, completionHandler:completionHandler)
    }
    
    func updateUserToCoreData(usersList:Array<Dictionary<String, String>>, completionHandler: (error:NSError?) -> Void) {
        
        let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let writeRequest = NSFetchRequest(entityName: "User")
        for user in usersList {
            let userObject = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext:managedObjectContext)
            
            userObject.setValue(user["firstName"], forKey: "firstName")
            userObject.setValue(user["emailId"], forKey: "emailId")
            userObject.setValue(user["imageUrl"], forKey: "imageUrl")
            userObject.setValue(user["lastName"], forKey: "lastName")
            do {
                try managedObjectContext.executeFetchRequest(writeRequest)
            } catch {
                print("Failed to update Coredata")
            }
        }
        completionHandler(error: nil)
    }
}
