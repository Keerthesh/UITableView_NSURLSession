//
//  User.swift
//  UITableView+NSURLSession
//
//  Created by Keerthesh on 29/06/16.
//  Copyright © 2016 Keerthesh. All rights reserved.
//

import Foundation
import CoreData


class User: NSManagedObject {

    class func fetchRequest() -> NSFetchRequest {
        
        return NSFetchRequest(entityName: "User")
    }
}
